package com.colruytgroup.productrecogcamera.controllers;

import android.content.Context;
import android.util.Log;
import com.colruytgroup.productrecogcamera.Camera;
import com.colruytgroup.productrecogcamera.services.CameraService;
import io.reactivex.rxjava3.core.Observable;
import ru.skornei.restserver.annotations.Accept;
import ru.skornei.restserver.annotations.Produces;
import ru.skornei.restserver.annotations.RestController;
import ru.skornei.restserver.annotations.methods.GET;
import ru.skornei.restserver.server.dictionary.ContentType;
import ru.skornei.restserver.server.dictionary.ResponseStatus;
import ru.skornei.restserver.server.protocol.RequestInfo;
import ru.skornei.restserver.server.protocol.ResponseInfo;

import java.util.Objects;
import java.util.concurrent.TimeUnit;


@RestController("/camera/pictures")
public class CameraPictureController {

    @GET
    @Produces(ContentType.TEXT_PLAIN)
    @Accept(ContentType.TEXT_PLAIN)
    public String getPictures(Context context, RequestInfo request, ResponseInfo response) {

        CameraService cameraService = CameraService.getInstance(context);

        try {
            if (request.getParameters().containsKey("camera") && Objects.requireNonNull(request.getParameters().get("camera")).size() > 0 ) {
                String cameraId = Objects.requireNonNull(request.getParameters().get("camera")).get(0);
                Camera camera = cameraService.getCameraById(cameraId);


                Observable<byte[]> cameraData$ = cameraService.takePicture(context, camera);

                byte[] imageData = cameraData$.timeout(10, TimeUnit.SECONDS).blockingLast();
                response.setBody(imageData);


            } else {
                response.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
                response.setBody("Specify a camera id using ?camera=".getBytes());

            }

        } catch (Exception ex) {
            Log.e("ProductRecognitionCamera", ex.getMessage(), ex);
            response.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
            if (ex.getMessage() != null) {
                response.setBody(ex.getMessage().getBytes());
            }
        }


        return "";

    }


}
