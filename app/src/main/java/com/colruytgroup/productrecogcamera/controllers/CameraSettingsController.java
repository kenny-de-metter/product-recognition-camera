package com.colruytgroup.productrecogcamera.controllers;

import android.content.Context;
import android.hardware.camera2.CameraCharacteristics;
import android.util.SizeF;
import com.colruytgroup.productrecogcamera.Camera;
import com.colruytgroup.productrecogcamera.services.CameraService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.skornei.restserver.annotations.Accept;
import ru.skornei.restserver.annotations.Produces;
import ru.skornei.restserver.annotations.RestController;
import ru.skornei.restserver.annotations.methods.GET;
import ru.skornei.restserver.server.dictionary.ContentType;
import ru.skornei.restserver.server.dictionary.ResponseStatus;
import ru.skornei.restserver.server.protocol.RequestInfo;
import ru.skornei.restserver.server.protocol.ResponseInfo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController("/camera/settings")
public class CameraSettingsController {



    public JSONObject getJsonSettings(Camera camera) throws JSONException {
        SizeF sensorSize = camera.getCharacteristics().get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE);
        float[] focalLengths = camera.getCharacteristics().get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS);

        String cameraFacing;

        switch (camera.getCharacteristics().get(CameraCharacteristics.LENS_FACING)) {
            case CameraCharacteristics.LENS_FACING_FRONT:
                cameraFacing = "LENS_FACING_FRONT";
                break;
            case CameraCharacteristics.LENS_FACING_BACK:
                cameraFacing ="LENS_FACING_BACK";
                break;
            default:
                cameraFacing = "UNKNOWN";
        }


        JSONObject jsonObj = new JSONObject();
        jsonObj.put("cameraId", camera.getCameraId());
        jsonObj.put("SENSOR_INFO_PHYSICAL_SIZE", sensorSize);

        JSONArray focalLengthsArray = new JSONArray();

        for (float f : focalLengths) {
            focalLengthsArray.put(f);
        }

        jsonObj.put("LENS_INFO_AVAILABLE_FOCAL_LENGTHS", focalLengthsArray);
        jsonObj.put("LENS_FACING", cameraFacing);
        jsonObj.put("REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT", isDepthCamera(camera));

        return jsonObj;

    }

    public boolean isDepthCamera(Camera camera) {
        int[] capabilities = camera.getCharacteristics().get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
        return Arrays.stream(capabilities).anyMatch((capability) -> capability == CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT);
    }


    @GET
    @Produces(ContentType.TEXT_PLAIN)
    @Accept(ContentType.TEXT_PLAIN)
    public String getSettings(Context context, RequestInfo request, ResponseInfo response) {


        if (request.getParameters().containsKey("camera")) {
            String cameraId = request.getParameters().get("camera").get(0);
            Camera camera = CameraService.getInstance(context).getCameraById(cameraId);

            try {
                JSONObject obj = getJsonSettings(camera);
                response.setBody(obj.toString().getBytes());
            } catch (JSONException ex) {
                response.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
                response.setBody(ex.getMessage().getBytes());
            }


        } else {

            Stream<Camera> cameras = CameraService.getInstance(context).getCameraDevices().stream();
            List<JSONObject> objs = cameras.map((Camera camera) -> {
                try {
                    return getJsonSettings(camera);
                } catch (JSONException ex) {
                      throw new RuntimeException(ex);
                }
            }).collect(Collectors.toList());
            JSONArray jsonArray = new JSONArray();
            for (JSONObject obj: objs) {
                jsonArray.put(obj);
            }

            response.setBody(jsonArray.toString().getBytes());

        }


        return "";

    }


}
