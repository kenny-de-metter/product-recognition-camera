package com.colruytgroup.productrecogcamera;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.colruytgroup.productrecogcamera.services.CameraService;

public class FirstFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);


    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        TextView timestamp = view.findViewById(R.id.timestamp);
        TextView cameraName = view.findViewById(R.id.camera_name);
        TextView cameraAction = view.findViewById(R.id.camera_action);

        CameraService.getInstance(this.getContext()).getCameraActivitySubject().subscribe((activity) -> {
            timestamp.setText(activity.getCurrentTime().toString());
            cameraName.setText("Camera " + activity.getCamera().getCameraId());
            cameraAction.setText(activity.getAction());

        }, (ex) -> {
            Log.e("ProductRecognitionCamera", ex.getMessage(), ex);
        });


    }
}