package com.colruytgroup.productrecogcamera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.hardware.camera2.*;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Size;
import android.util.SizeF;
import android.view.Surface;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import com.colruytgroup.productrecogcamera.services.CameraService;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import org.json.JSONArray;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Camera extends CameraDevice.StateCallback {
    private String cameraId;
    private CameraCharacteristics characteristics;

    private Subject<CameraActivity> cameraActivitySubject;
    private Subject<byte[]> cameraData$ = PublishSubject.create();

    public CameraCharacteristics getCharacteristics() {
        return characteristics;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public String getCameraId() {
        return cameraId;
    }

    public void setCharacteristics(CameraCharacteristics characteristics) {
        this.characteristics = characteristics;
    }

    public Observable<byte[]> openCamera(Context context) throws CameraAccessException {

        this.cameraActivitySubject = CameraService.getInstance(context).getCameraActivitySubject();

        Handler handler = new Handler(Looper.getMainLooper());

        CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);


        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            Log.v("ProductRecognitionCamera", "Camera " + this.getCameraId() + " has permission");
            cameraManager.openCamera(this.getCameraId(), this, handler);

        } else {
            Log.e("ProductRecognitionCamera", "Camera " + this.getCameraId() + " doesn't have permission");
        }

        return this.cameraData$;

    }

    public boolean isDepthCamera() {
        int[] capabilities = this.getCharacteristics().get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
        return Arrays.stream(capabilities).anyMatch((capability) -> capability == CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT);
    }


    @Override
    public void onOpened(@NonNull CameraDevice camera) {
        try {

            Log.v("ProductRecognitionCamera", "OPENED CAMERA " + camera.getId());
            logActivity("OPENED");

            CaptureRequest.Builder builder = camera.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

            ImageReader reader;
            Size size;

            if (this.isDepthCamera()) {
                Log.v("ProductRecognitionCamera", "OPENED CAMERA " + camera.getId());
                size = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.DEPTH16)[0];
                reader = ImageReader.newInstance(size.getWidth(), size.getHeight(), ImageFormat.DEPTH16, 1);

                SizeF sensorSize = this.characteristics.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE);
                float[] focalLengths = this.characteristics.get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS);

                if (focalLengths.length > 0) {
                    float focalLength = focalLengths[0];
                    double fov = 2 * Math.atan(sensorSize.getWidth() / (2 * focalLength));
                    Log.v("ProductRecognitionCamera", "Calculated FOV :" + fov);
                }

            } else {
                size = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG)[0];
                reader = ImageReader.newInstance(size.getWidth(), size.getHeight(), ImageFormat.JPEG, 1);
            }


            builder.addTarget(reader.getSurface());
            List<Surface> surfaces = Collections.singletonList(reader.getSurface());

            camera.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    try {
                        Log.v("ProductRecognitionCamera", "CREATE CAPTURE SESSION FOR CAMERA " + camera.getId());
                        session.capture(builder.build(), null, null);
                        session.close();
                    } catch (Exception ex) {
                        Log.e("ProductRecognitionCamera", ex.getMessage(), ex);
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Log.e("ProductRecognitionCamera", "Something went wrong for camera " + camera.getId());
                    session.close();
                }
            }, null);


            reader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {

                    Image image = reader.acquireNextImage();
                    byte[] result;


                    if (isDepthCamera()) {
                        int[][] data = processImage(image, size);

                        JSONArray jsonArray = new JSONArray();

                        for (int y = 0; y < data.length; y++) {
                            int[] row = data[y];
                            JSONArray rowJsonArray = new JSONArray();
                            for (int x = 0; x < row.length; x++) {
                                rowJsonArray.put(row[x]);
                            }
                            jsonArray.put(rowJsonArray);
                        }

                        result = jsonArray.toString().getBytes();


                    } else {

                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.remaining()];
                        buffer.get(bytes);
                        result = bytes;

                    }

                    logActivity("IMAGE TAKEN");
                    image.close();
                    camera.close();

                    cameraData$.onNext(result);
                    cameraData$.onComplete();


                }
            }, null);


        } catch (CameraAccessException ex) {
            Log.e("ProductRecognitionCamera", ex.getMessage(), ex);
        }


    }

    public void logActivity(String activity) {
        this.cameraActivitySubject.onNext(new CameraActivity(this, activity));
    }


    @Override
    public void onDisconnected(@NonNull CameraDevice camera) {

    }

    @Override
    public void onError(@NonNull CameraDevice camera, int error) {

    }


    /**
     * Process the depth map image, returning a 2 dimensional array (rows & columns)
     *
     * @param image
     * @param size
     * @return
     */

    private int[][] processImage(Image image, Size size) {
        ShortBuffer shortDepthBuffer = image.getPlanes()[0].getBuffer().asShortBuffer();

        int WIDTH = size.getWidth();
        int HEIGHT = size.getHeight();

        int[][] mask = new int[HEIGHT][WIDTH];

        for (int y = 0; y < HEIGHT; y++) {
            int[] row = new int[WIDTH];
            for (int x = 0; x < WIDTH; x++) {
                int index = y * WIDTH + x;
                short depthSample = shortDepthBuffer.get(index);
                row[x] = depthSample;
            }
            mask[y] = row;
        }

        return mask;

    }

}
