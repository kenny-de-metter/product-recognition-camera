package com.colruytgroup.productrecogcamera.services;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.os.Looper;
import com.colruytgroup.productrecogcamera.Camera;
import com.colruytgroup.productrecogcamera.CameraActivity;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CameraService {


    private static CameraService instance;
    private CameraManager cameraManager;


    private PublishSubject<CameraActivity> cameraActivitySubject = PublishSubject.create();

    public static CameraService getInstance(Context context) {

        if (instance == null) {
            instance = new CameraService(context);
        }

        return instance;
    }


    private CameraService(Context context) {
        this.cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
    }


    public List<Camera> getCameraDevices() {
        try {
            return Arrays.stream(cameraManager.getCameraIdList()).map(this::getCameraById).collect(Collectors.toList());
        } catch (CameraAccessException ex) {
            throw new RuntimeException(ex);
        }

    }


    public Camera getCameraById(String id) {

        try {
            Camera camera = new Camera();
            camera.setCameraId(id);
            camera.setCharacteristics(cameraManager.getCameraCharacteristics(id));
            return camera;
        } catch (CameraAccessException e) {
            throw new RuntimeException(e);
        }

    }

    public Observable<byte[]> takePicture(Context context, Camera camera) throws CameraAccessException {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        return camera.openCamera(context);

    }


    public PublishSubject<CameraActivity> getCameraActivitySubject() {
        return cameraActivitySubject;
    }
}
