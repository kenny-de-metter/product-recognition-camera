package com.colruytgroup.productrecogcamera;

import android.app.Application;
import ru.skornei.restserver.RestServerManager;

public class CameraApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        RestServerManager.initialize(this);

    }



}
