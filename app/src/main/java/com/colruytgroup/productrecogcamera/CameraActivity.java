package com.colruytgroup.productrecogcamera;

import java.time.Instant;

public class CameraActivity {
    private Camera camera;
    private Instant currentTime;
    private String action;


    public CameraActivity(Camera camera, String action) {
        this.camera = camera;
        this.currentTime = Instant.now();
        this.action = action;
    }

    public Camera getCamera() {
        return camera;
    }

    public Instant getCurrentTime() {
        return currentTime;
    }

    public String getAction() {
        return action;
    }



}
