package com.colruytgroup.productrecogcamera;

import com.colruytgroup.productrecogcamera.controllers.CameraPictureController;
import com.colruytgroup.productrecogcamera.controllers.CameraSettingsController;
import ru.skornei.restserver.annotations.RestServer;
import ru.skornei.restserver.server.BaseRestServer;

@RestServer(port = MainServer.PORT, controllers = {CameraPictureController.class, CameraSettingsController.class})
public class MainServer extends BaseRestServer {
    public static final int PORT = 8080;



}
